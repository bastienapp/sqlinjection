# SQL Injection Vulnerability Test

This repository demonstrates a simple example of an SQL injection vulnerability.

The goal is to show how an SQL injection attack can be performed and how to fix the code to prevent such vulnerabilities.

Additionally, it covers best practices to secure your code.

## Installation

To set up the server, follow these steps:

1. Build the Docker image:

```bash
docker build -t php-sqlinjection-image .
```

2. Run the Docker container:

```bash
docker run -d -p 8080:80 --name php-sqlinjection-container -v "$(pwd):/var/www/html" php-sqlinjection-image
```

Access the application in your browser at `http://localhost:8080`.

## SQL Injection Vulnerability Test

### Step 1: Testing the Vulnerability

1. Open the login page.
2. Attempt to perform an SQL injection attack to bypass the login. The goal is to understand how the vulnerability can be exploited.

### Step 2: Fixing the Code

Modify the code in `login.php` to secure it against SQL injection. The goal is to prevent malicious inputs from altering the SQL queries in an unintended way.
